#include <iostream> // ������ �����-������
#include <chrono> // ������ �� ��������
#include <thread> // ������ ����������
#include <mutex> // ��������
#include <string> // ������
#include <windows.h> 
#include <iostream> 



std::mutex g_mutex;// ���������� ���������� ��������
bool g_ready = false;
int g_data = 0;



int produceData() {
     int randomNumber = rand() % 9; // ��������� ���������� �����

     std::cout << "\n���������� �����: " << randomNumber << "\n"; // ����� ����� �����
    
    return randomNumber;
}

void consumeData(int data) { 
    std::cout << "�������������� ����������: ";

    while (data < 500) { // ������� ����������� ������
       
        std::cout << (data *= 2) << " "; // ����� ������������������ �����
        
    }
   
}


void  thread_functionOne() {
    for (int i = 0; i < 5; i++) {
        while (!g_ready) {
          
                // ��� � ������� 1 ���
             std::this_thread::sleep_for(std::chrono::seconds(1));
        
        }
        std::unique_lock<std::mutex> ul(g_mutex); // ���������� ������������ ������� 
        consumeData(g_data); // ������������� ������
      
        g_ready = false; // ��������� �����
    }
}


void  thread_functionTwo() {
    for (int i = 0; i < 5; i++) {

        std::unique_lock<std::mutex> ul(g_mutex); // ���������� ������������ �������

        g_data = produceData(); // ��������� ������
        g_ready = true; // ��������� ��� �����
        ul.unlock(); // �������������
        while (g_ready) {
           
                // ��� � ������� 1 ���
                std::this_thread::sleep_for(std::chrono::seconds(1));
          
        }
    }
}

// ���������� �������
void TwoThread() { thread_functionTwo(); }

void OneThread() { thread_functionOne(); }

int main() {

    // ��� ������ ��������� � �������
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    // ������� �������
    std::thread thrd(TwoThread); 
    std::thread thrd2(OneThread);
    // �������� ����������
    thrd.join();
    thrd2.join();
    return 0;
}